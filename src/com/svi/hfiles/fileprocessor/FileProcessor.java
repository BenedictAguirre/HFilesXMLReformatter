package com.svi.hfiles.fileprocessor;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.svi.hfiles.config.Config;
import com.svi.hfiles.pdfcopier.PdfCopier;
import com.svi.hfiles.xmlreformatter.XmlReformatter;

public class FileProcessor {

	public static void main(String[] args) throws IOException, ParseException {
		FileProcessor fp = new FileProcessor();
		fp.browseDir();
	}

	public void browseDir() throws IOException, ParseException {
		File dir = new File(Config.getProperty("INPUT_PATH"));
		XmlReformatter xmlreformatter = new XmlReformatter();
		File[] directoryListing = dir.listFiles();
		for (File file : directoryListing) {
			String dtaFilePath = null;
			String outputMainDir = null;
			String newdate = null;
			String fileName = null;
			if (file.isFile()) {
				if (file.getName().contains(Config.getProperty("INPUT_FILE_EXTENSION"))) {
					fileName = file.getName().substring(0, file.getName().indexOf('.'));
					String[] data = fileName.split("_");
					SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd");
					Date date = formatter.parse(data[0]);
					SimpleDateFormat newformatter = new SimpleDateFormat("MM-dd-yyyy");
					newdate = newformatter.format(date);
//					System.out.println(newdate);
					outputMainDir = data[1] + "_" + data[2] + "_" + data[3] + "_" + data[4];
					dtaFilePath = file.getAbsolutePath();
					xmlreformatter.getRawXmlData(file.getAbsolutePath(), outputMainDir, newdate);
					System.out.println("Successfully reformatted this file : " + fileName);
				}
			}
			List<Path> result = null;
			Path path = Paths.get(Config.getProperty("ELEMENT_PATH"));
			try (Stream<Path> walk = Files.walk(path )) {
				result = walk.filter(Files::isRegularFile).collect(Collectors.toList());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			for (Path path2 : result) {
				if (path2.toString().contains(fileName)&&path2.toString().endsWith(Config.getProperty("LST_EXT_NAME"))) {
					System.out.println(path2.toString());
					String lstPath = path2.toString();
					PdfCopier pdfCopier = new PdfCopier();
					pdfCopier.copyPdfToOutputPath(lstPath, dtaFilePath, outputMainDir, newdate, getErrorLogfilePath());
				}
//				System.out.println(path2.toString());
			}

		}
	}
	private static String getErrorLogfilePath() {
		Date date = new Date();
		String fileDelimiter = "/";
		SimpleDateFormat formatter = new SimpleDateFormat("MMddyyyy_hhmmsss");
		String currentDate = formatter.format(date);
		String fileName = "XML_Reformatter_Errors_" + currentDate;
		File errorDir = new File(Config.getProperty("ERROR_LOG_PATH"));
		if (!errorDir.exists()) {
			errorDir.mkdir();
		}
		String filePath = errorDir.getAbsolutePath() + fileDelimiter + fileName + ".txt";
		return filePath;
	}
}
