package com.svi.hfiles.fileprocessor;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String fileName = "AGREEMENT FOR PROJECT EMPLOYMENT03-02-2020.xml";
		 List<Path> result = new ArrayList<>();
		 String rawFileName = fileName.substring(0, fileName.indexOf('.'));
		 String fileExtension = fileName.substring( fileName.indexOf('.'), fileName.length());
			try (Stream<Path> walk = Files.walk(Paths.get("C:\\Users\\dimay\\eclipse-workspace\\HFilesXMLReformatter\\resources\\test\\020070_MORANA_ZENY ANN_G\\XML"))) {
				 result = walk.filter(Files::isRegularFile).collect(Collectors.toList());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			List<File> duplicateFiles = new ArrayList<>();
			boolean dontExist = true;
			for (Path path : result) {
				File i = new File(path.toString());
				if (i.getName().contains(fileName.substring(0, fileName.indexOf('.')))) {
					duplicateFiles.add(i);
					dontExist = false;
				}
			}
			if (!dontExist) {
				fileName = rawFileName+"_"+String.format("%02d" , duplicateFiles.size())+fileExtension;
			}else {
				fileName = rawFileName+fileExtension;
			}
			System.out.println(fileName);
	}

}
