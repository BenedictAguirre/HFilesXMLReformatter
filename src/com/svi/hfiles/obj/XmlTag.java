package com.svi.hfiles.obj;

public class XmlTag {

	private String tagName;
	private String tagValue;
	private int id;
	private String parentNode;

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tag) {
		this.tagName = tag;
	}

	public String getTagValue() {
		return tagValue;
	}

	public void setTagValue(String value) {
		this.tagValue = value;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getParentNode() {
		return parentNode;
	}

	public void setParentNode(String parentNode) {
		this.parentNode = parentNode;
	}

	public String toString() {
		return tagName + " " + tagValue + " " + id + " " + parentNode;

	}
}
