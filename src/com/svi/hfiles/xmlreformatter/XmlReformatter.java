package com.svi.hfiles.xmlreformatter;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import com.svi.hfiles.config.Config;
import com.svi.hfiles.dbutils.MsAccessDBConnUtils;
import com.svi.hfiles.dbutils.MsAccessDBUtils;
import com.svi.hfiles.obj.XmlTag;

public class XmlReformatter {

	public void getRawXmlData(String path, String outputMainDir, String date) {
		MsAccessDBUtils msAccess = new MsAccessDBUtils();
		MsAccessDBConnUtils dbConn = new MsAccessDBConnUtils();
		String fileName = null;
		try {
			Connection conn = dbConn.intializeAccessDB();
//			File inputFile = new File(Config.getProperty("INPUT_PATH") + "/20200302_020070_MORANA_ZENY ANN_G_001.DTA");
			XmlTag xmlTag = new XmlTag();
			ArrayList<XmlTag> xmlTagList = new ArrayList<>();

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(path);
			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName("field");
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;

					if (!eElement.getElementsByTagName("value").item(0).getTextContent().equals("0")) {
						if (eElement.getAttribute("no").equals("1")) {
//							System.out.println(eElement.getElementsByTagName("value").item(0).getTextContent());
							fileName = eElement.getElementsByTagName("value").item(0).getTextContent() + date +Config.getProperty("OUTPUT_FILE_EXTENSION");
						}
						String value = eElement.getElementsByTagName("value").item(0).getTextContent();
						xmlTag = msAccess.retrieve(conn, eElement.getAttribute("no"), value);
						xmlTagList.add(xmlTag);
					}
				}
			}

			Map<String, XmlTag> xmlTagMap = new HashMap<>();
			for (XmlTag value : xmlTagList) {
				if (value.getTagName().equalsIgnoreCase("doctype_id")) {
//					System.out.println(value.getTagValue());
				}
				// System.out.println(value.getParentNode()+ " "+ value.getTagName());
				if (value.getParentNode() != null) {
					String[] temp = value.getParentNode().split("/");
					String parentNode = temp[temp.length - 1];
					String key = parentNode + "_" + value.getTagName();
					if (!xmlTagMap.containsKey(key)) {
						xmlTagMap.put(key, value);
					}
				}

			}
//			reformatXml(tags, fileName);
			reformatXml(xmlTagMap, fileName, outputMainDir);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	static void reformatXml(Map<String, XmlTag> xmlTagMap, String fileName, String outputMainDir)
			throws SAXException, IOException, ParserConfigurationException, TransformerException {
		String xmlInfo;
		String employeeNo = "";
		String firstName = "";
		String lastName = "";
		String middleName = "";
		// root element
		String fileDelimiter = "/";
		String xmlDir = "XML";
		String xmlFilePath = Config.getProperty("TEMPLATE_PATH");
		String outputRootPath = Config.getProperty("OUTPUT_ROOT_PATH");
		String lastIndex = outputRootPath.substring(outputRootPath.length()-1);
		if(!lastIndex.equals(fileDelimiter)) {
			outputRootPath = outputRootPath + fileDelimiter;
//			System.out.println(outputRootPath);
		}
		
		DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();

		DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();

		Document document = documentBuilder.parse(xmlFilePath);
		NodeList nodeList = document.getElementsByTagName("*");


		for (int temp = 0; temp < nodeList.getLength(); temp++) {
			Node nNode = nodeList.item(temp);
			if (nNode.getNodeType() == Node.ELEMENT_NODE) {
				Element eElement = (Element) nNode;
				String nodeName = eElement.getNodeName();
				String key = eElement.getParentNode().getNodeName() + "_" + nodeName;
				
				if (xmlTagMap.containsKey(key)) {
					XmlTag xml = xmlTagMap.get(key);
					if (xml.getTagName().equals("first_name")) {
//						System.out.println("First Name: " + xml.getTagValue());
						firstName = "_" + xml.getTagValue();
					}
					if (xml.getTagName().equalsIgnoreCase("last_name")) {
//						System.out.println("Last Name: " + xml.getTagValue());
						lastName = "_" + xml.getTagValue();
					}
					if (xml.getTagName().equalsIgnoreCase("middle_name")) {
						if (xml.getTagValue() != null || !xml.getTagValue().equalsIgnoreCase("")) {
//							System.out.println("Middle Name: " + xml.getTagValue().substring(0, 1));
							middleName = "_" + xml.getTagValue().substring(0, 1);
						}

					}
					if (xml.getTagName().equalsIgnoreCase("employee_no")
							|| xml.getTagName().equalsIgnoreCase("employee_id")) {
//						System.out.println("Employee ID: " + xml.getTagValue());
						employeeNo = xml.getTagValue();
					}
					eElement.setTextContent(xml.getTagValue());
				}
			}
		}
		xmlInfo = employeeNo + lastName + firstName + middleName;
//		System.out.println(xmlInfo);
		// St
//		File infoDir = new File(outputRootPath + xmlInfo);
		File infoDir = new File(outputRootPath + outputMainDir);
		if (!infoDir.exists()) {
			infoDir.mkdir();
		}

		File xmlDirectory = new File(infoDir.getAbsoluteFile().toString() + fileDelimiter + xmlDir);
		if (!xmlDirectory.exists()) {
			xmlDirectory.mkdir();
		}
//		System.out.println(xmlDirectory.getAbsolutePath());
		fileName = renameFilenameIfHasDuplicate(fileName, xmlDirectory);
		String outputPath = xmlDirectory.getAbsolutePath() + fileDelimiter + fileName;
		
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
		DOMSource domSource = new DOMSource(document);
		StreamResult streamResult = new StreamResult(new File(outputPath));
		transformer.transform(domSource, streamResult);

	}
	 private static String renameFilenameIfHasDuplicate(String fileName, File xmlDirectory) {
		 List<Path> result = new ArrayList<>();
		 String rawFileName = fileName.substring(0, fileName.indexOf('.'));
		 String fileExtension = fileName.substring( fileName.indexOf('.'), fileName.length());
			try (Stream<Path> walk = Files.walk(Paths.get(xmlDirectory.getAbsolutePath()))) {
				 result = walk.filter(Files::isRegularFile).collect(Collectors.toList());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			List<File> duplicateFiles = new ArrayList<>();
			boolean dontExist = true;
			for (Path path : result) {
				File i = new File(path.toString());
				if (i.getName().contains(fileName.substring(0, fileName.indexOf('.')))) {
					duplicateFiles.add(i);
					dontExist = false;
				}
			}
			if (!dontExist) {
				fileName = rawFileName+"_"+String.format("%02d" , duplicateFiles.size())+fileExtension;
			}else {
				fileName = rawFileName+fileExtension;
			}
		return fileName;
		 
	 }
}
