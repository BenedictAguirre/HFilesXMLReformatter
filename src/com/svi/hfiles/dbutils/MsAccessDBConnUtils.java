package com.svi.hfiles.dbutils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import com.svi.hfiles.config.Config;


public class MsAccessDBConnUtils {
	private static MsAccessDBConnUtils msAccess = new MsAccessDBConnUtils();
//	public static void main(String[] args) throws SQLException {
//		msAccess.retrieveAll();
//	}
	public Connection intializeAccessDB() {
		Connection connection = null;
		Statement statement = null;

		try {
			Class.forName(Config.getProperty("CLASS_FORNAME"));
			String dbURL = Config.getProperty("JDBC") + Config.getProperty("DATABASE_PATH");

			// Step 2.A: Create and
			// get connection using DriverManager class
			connection = DriverManager.getConnection(dbURL);

			// Step 2.B: Creating JDBC Statement
			statement = connection.createStatement();
	} catch(Exception ex)
    {
        ex.printStackTrace();
    }
		
    return connection;
	}
}