package com.svi.hfiles.dbutils;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import com.svi.hfiles.config.Config;
import com.svi.hfiles.obj.XmlTag;

public class MsAccessDBUtils {
	public XmlTag retrieve(Connection conn, String field, String value) {
		XmlTag xmlTag = new XmlTag();
		ResultSet resultSet = null;
		try {
			Statement statement = conn.createStatement();
			String queryFields = Config.getProperty("COLUMN1") + "," + Config.getProperty("COLUMN2");
			resultSet = statement.executeQuery("SELECT " + queryFields + " FROM " + Config.getProperty("MAIN_TABLE")
					+ " Where " + Config.getProperty("COLUMN_FILTER") + " = " + field);
			if (resultSet != null) {
				while (resultSet.next()) {
					String fieldNo = resultSet.getString(1);

					String parentNode = resultSet.getString(2);
					// xmlTags.put(fieldNo, value);
					xmlTag.setTagName(fieldNo);
					xmlTag.setTagValue(value);
					xmlTag.setId(Integer.parseInt(field));
					xmlTag.setParentNode(parentNode);

				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// return companies;
		return xmlTag;
	}
}
