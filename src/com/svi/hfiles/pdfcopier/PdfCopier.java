package com.svi.hfiles.pdfcopier;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.apache.pdfbox.exceptions.COSVisitorException;
import org.apache.pdfbox.util.PDFMergerUtility;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.svi.hfiles.config.Config;
import com.svi.hfiles.dbutils.MsAccessDBConnUtils;
import com.svi.hfiles.dbutils.MsAccessDBUtils;
import com.svi.hfiles.obj.XmlTag;

public class PdfCopier {
	

	private String getPdfCopyOutputPath(String outputMainDir, String fileName, String dpi) {

		String outputPath = null;
		String outputRootPath = Config.getProperty("OUTPUT_ROOT_PATH");
		String fileDelimiter = "/";
		String lastIndex = outputRootPath.substring(outputRootPath.length() - 1);
		if (!lastIndex.equals(fileDelimiter)) {
			outputRootPath = outputRootPath + fileDelimiter;
//			System.out.println(outputRootPath);
		}
		File infoDir = new File(outputRootPath + outputMainDir);
		if (!infoDir.exists()) {
			infoDir.mkdir();
		}

		File pdfDirectory = new File(infoDir.getAbsoluteFile().toString() + fileDelimiter + dpi);
		if (!pdfDirectory.exists()) {
			pdfDirectory.mkdir();
		}
		fileName = renameFilenameIfHasDuplicate(fileName, pdfDirectory);
		outputPath = pdfDirectory.getAbsolutePath() + fileDelimiter + fileName;
		return outputPath;
	}
	private String renameFilenameIfHasDuplicate(String fileName, File xmlDirectory) {
		 List<Path> result = new ArrayList<>();
		 String rawFileName = fileName.substring(0, fileName.indexOf('.'));
		 String fileExtension = fileName.substring( fileName.indexOf('.'), fileName.length());
			try (Stream<Path> walk = Files.walk(Paths.get(xmlDirectory.getAbsolutePath()))) {
				 result = walk.filter(Files::isRegularFile).collect(Collectors.toList());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			List<File> duplicateFiles = new ArrayList<>();
			boolean dontExist = true;
			for (Path path : result) {
				File i = new File(path.toString());
				if (i.getName().contains(fileName.substring(0, fileName.indexOf('.')))) {
					duplicateFiles.add(i);
					dontExist = false;
				}
			}
			if (!dontExist) {
				fileName = rawFileName+"_"+String.format("%02d" , duplicateFiles.size())+fileExtension;
			}else {
				fileName = rawFileName+fileExtension;
			}
		return fileName;
		 
	 }

	private String getFileName(String dtaFilePath, String date) {
		MsAccessDBUtils msAccess = new MsAccessDBUtils();
		MsAccessDBConnUtils dbConn = new MsAccessDBConnUtils();
		String fileName = null;
		try {
			Connection conn = dbConn.intializeAccessDB();
//			File inputFile = new File(Config.getProperty("INPUT_PATH") + "/20200302_020070_MORANA_ZENY ANN_G_001.DTA");
			XmlTag xmlTag = new XmlTag();
			ArrayList<XmlTag> xmlTagList = new ArrayList<>();

			DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
			DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
			Document doc = dBuilder.parse(dtaFilePath);
			doc.getDocumentElement().normalize();
			NodeList nList = doc.getElementsByTagName("field");
			for (int temp = 0; temp < nList.getLength(); temp++) {
				Node nNode = nList.item(temp);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {
					Element eElement = (Element) nNode;

					if (!eElement.getElementsByTagName("value").item(0).getTextContent().equals("0")) {
						if (eElement.getAttribute("no").equals("1")) {
//							System.out.println(eElement.getElementsByTagName("value").item(0).getTextContent());
							fileName = eElement.getElementsByTagName("value").item(0).getTextContent() + date
									+ Config.getProperty("PDF_FILE_EXTENSION");
						}
						String value = eElement.getElementsByTagName("value").item(0).getTextContent();
						xmlTag = msAccess.retrieve(conn, eElement.getAttribute("no"), value);
						xmlTagList.add(xmlTag);
					}
				}
			}
//			reformatXml(tags, fileName);

		} catch (Exception e) {
			e.printStackTrace();
		}
		return fileName;
	}

	private List<String> getLSTFileContent(String lstFilePath) {
		List<String> arrayList = new ArrayList<>();
		try (BufferedReader reader = new BufferedReader(new FileReader(lstFilePath))) {
			while (reader.ready()) {
				arrayList.add(reader.readLine());
			}
		} catch (IOException e) {
			// Handle a potential exception
		}
		return arrayList;
	}

	private String getLSTFileNameContent(String LSTContent) {
		String fileName = null;
		if (LSTContent != null) {
			File lstFile = new File(LSTContent);
			fileName = lstFile.getName().substring(0, lstFile.getName().indexOf('.'));
		}
		return fileName;

	}

	private String getLSTParentFolderContent(String LSTContent) {
		String parentFolderPath = null;
		if (LSTContent != null) {
			File lstFile = new File(LSTContent);
			File parent = new File(lstFile.getParent());
			parentFolderPath = parent.getName();
		}
		return parentFolderPath;
	}

	private void writeError(String errorLog, String message) {
		File errorLogFile = new File(errorLog);
		if (!errorLogFile.exists()) {
			try {
				errorLogFile.createNewFile();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		try {
			Files.write(Paths.get(errorLog), message.getBytes(), StandardOpenOption.APPEND);
		} catch (IOException e) {
			// exception handling left as an exercise for the reader
		}
	}
	private List<String> getPdfFileNames(List<String> contents,List<Path> result, String dtaFilePath, String parentFolderPath, String errorLog, String dpi){
		List<String> pdfdpis = new ArrayList<>();
		for (String content : contents) {
			String pdfdpi = null;
			for (Path path : result) {
				File i = new File(path.toString());
//				File parent = new File(i.getParent());
				if (i.getName().contains(getLSTFileNameContent(content))&&i.getName().endsWith(Config.getProperty("PDF_FILE_EXTENSION"))) {
					pdfdpi = path.toString();
					pdfdpis.add(pdfdpi);
				}
			}
			if (pdfdpi == null) {
				File dta = new File(dtaFilePath);
				String message = "Missing "+dpi+" dpi PDF File for: " + dta.getName() + " from folder: " + parentFolderPath
						+ "\n";
				writeError(errorLog, message);
			}
			

		}
		return pdfdpis;
	}

	private List<File> getPdfFiles(List<String> pdfdpis) {
		List<File> pdfFiles = new ArrayList<>();
		for (String pdfdpi : pdfdpis) {
			
				File pdf = new File(pdfdpi);
				pdfFiles.add(pdf);
			
		}
		return pdfFiles;
	}

	private void createFinalPdfFile(List<File> pdfFiles, String outputMainDir, String fileName, String dpi) {
		PDFMergerUtility PDFmerger = new PDFMergerUtility();
		PDFmerger.setDestinationFileName(getPdfCopyOutputPath(outputMainDir, fileName, dpi));
		for (File file : pdfFiles) { 
			PDFmerger.addSource(file);
		}
		try {
			PDFmerger.mergeDocuments();
			System.out.println("Successfully copied pdf : "+getPdfCopyOutputPath(outputMainDir, fileName, dpi));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (COSVisitorException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	public void copyPdfto72dpiFolder(String lstFilePath, String outputMainDir, String date, String dtaFilePath,
			String errorLog) {
		List<String> contents = getLSTFileContent(lstFilePath);
		String parentFolderPath = Config.getProperty("72_DPI_PDF_SOURCE_PATH") + "/"
				+ getLSTParentFolderContent(contents.get(0));
		Path path = Paths.get(parentFolderPath);
		List<Path> result = null;
		try (Stream<Path> walk = Files.walk(path)) {
			result = walk.filter(Files::isRegularFile).collect(Collectors.toList());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String dpi = "72DPI";
		List<String> pdf72dpis = getPdfFileNames(contents, result, dtaFilePath, parentFolderPath, errorLog, dpi);
		List<File> pdfFiles = getPdfFiles(pdf72dpis);
		String fileName = getFileName(dtaFilePath, date);
		createFinalPdfFile(pdfFiles, outputMainDir, fileName, dpi);

	}

	public void copyPdfto300dpiFolder(String lstFilePath, String outputMainDir, String date, String dtaFilePath,
			String errorLog) {
		List<String> contents = getLSTFileContent(lstFilePath);
		String parentFolderPath = Config.getProperty("300_DPI_PDF_SOURCE_PATH") + "/"
				+ getLSTParentFolderContent(contents.get(0));
		Path path = Paths.get(parentFolderPath);
		List<Path> result = null;
		try (Stream<Path> walk = Files.walk(path)) {
			result = walk.filter(Files::isRegularFile).collect(Collectors.toList());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		String dpi = "300DPI";
		List<String> pdf300dpis = getPdfFileNames(contents, result, dtaFilePath, parentFolderPath, errorLog,dpi);
		List<File> pdfFiles = getPdfFiles(pdf300dpis);
		String fileName = getFileName(dtaFilePath, date);	
		createFinalPdfFile(pdfFiles, outputMainDir, fileName, dpi);

	}

	public void copyPdfToOutputPath(String lstFilePath, String dtaFilePath, String outputMainDir, String date, String errorLogFile) {
		copyPdfto72dpiFolder(lstFilePath, outputMainDir, date, dtaFilePath, errorLogFile);
		copyPdfto300dpiFolder(lstFilePath, outputMainDir, date, dtaFilePath, errorLogFile);
	}
//	public static void main(String[] args){
////		PdfCopier p = new PdfCopier();
////		p.copyPdfToOutputPath("C:\\Users\\dimay\\Downloads\\sample_element_folder_20200302_020070_MORANA_ZENY ANN_G_001\\20200302_020070_MORANA_ZENY ANN_G_001\\20200302_020070_MORANA_ZENY ANN_G_001.LST", "C:\\Users\\dimay\\Downloads\\sample_element_folder_20200302_020070_MORANA_ZENY ANN_G_001\\20200302_020070_MORANA_ZENY ANN_G_001\\20200302_020070_MORANA_ZENY ANN_G_001.DTA", "TEST_TEST_TEST_TEST", "03-02-2020");
//	}

}
